//
//  ViewControllerItem1.h
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 11/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewControllerItem1 : UIViewController {
    IBOutlet UITextField *tfEmail;
    IBOutlet UITextField *tfUser;
    IBOutlet UITextField *tfPassword;
    IBOutlet UILabel *lblEmail;
    IBOutlet UILabel *lblUser;
    IBOutlet UILabel *lblPassword;
    IBOutlet UIButton *btnAccept;
    IBOutlet UIButton *btnCancel;
    IBOutlet UIImageView *image;
}

@end
