//
//  ViewControllerItem1.m
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 14/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import "ViewControllerItem1.h"
#import "AppDelegate.h"

@interface ViewControllerItem1 ()

@end

@implementation ViewControllerItem1

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCellItem1 *cell = (TableViewCellItem1*)[tableView dequeueReusableCellWithIdentifier:@"table1cell"];
    NSInteger *row = [indexPath row];
    num = 0;
    
    NSString* str=[NSString stringWithFormat:@"Celda %ld",row];
    [cell setTexto: str];
    /*if (row == 0) {
        [cell setTexto: @"Celda 1"];
    } else if(row == 1) {
        [cell setTexto: @"Celda 2"];
    } else if(row == 2) {
        [cell setTexto: @"Celda 3"];
    } else if(row == 3) {
        [cell setTexto: @"Celda 4"];
    } else if(row == 4)
        [cell setTexto: @"Celda 5"];*/
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

@end
