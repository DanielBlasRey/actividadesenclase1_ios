//
//  ViewController.m
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 11/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (AppDelegate *)getAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(IBAction)accionDelBotonLogIn:(id)sender{
    AppDelegate *aDeleg = [self getAppDelegate];
    User *userTemp = [aDeleg getUser];
    
    if ([[userTemp getName] isEqualToString:tfUser.text] && [[userTemp getPass] isEqualToString:tfPassword.text]) {
         [self performSegueWithIdentifier:@"LogIntoTab" sender:self];
    } else {
        UIAlertController *popUpController = [UIAlertController alertControllerWithTitle:@"¡Error!" message:@"Los datos no coinciden." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *popUpAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDestructive handler:nil];
        [popUpController addAction:popUpAction];
        
        [self presentViewController:popUpController animated:YES completion:nil];
        
    }
}

@end
