//
//  ViewControllerRegistrar.m
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 12/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import "ViewControllerRegistrar.h"
#import "AppDelegate.h"

@interface ViewControllerRegistrar ()

@end

@implementation ViewControllerRegistrar

- (AppDelegate *)getAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(IBAction)accionRegistrar:(id)sender {
    if (tfPassword.text.length >= 8) {
        AppDelegate *aDeleg = [self getAppDelegate];
        User *userTemp = [aDeleg getUser];
        [userTemp setMail:tfEmail.text];
        [userTemp setName:tfUser.text];
        [userTemp setPass:tfPassword.text];
        [self performSegueWithIdentifier:@"RegistertoLogIn" sender:self];
    }
   
    
    
    
}


@end
