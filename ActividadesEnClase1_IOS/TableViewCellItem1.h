//
//  TableViewCellItem1.h
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 14/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TableViewCellItem1 : UITableViewCell {
    IBOutlet UIImageView *ivImage;
    IBOutlet UILabel *lblTexto;

}

-(void)setTexto:(NSString*)texto;

@end
