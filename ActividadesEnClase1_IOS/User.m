//
//  User.m
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 14/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import "User.h"

@implementation User

-(void)setMail:(NSString*)varMail {
    mail = varMail;
    NSLog(@"SET MAIL %@", varMail);
}

-(NSString*)getMail {
    NSLog(@"SET MAIL %@", mail);
    return mail;
}

-(void)setName:(NSString*)varName {
    name = varName;
    NSLog(@"SET NOMBRE %@", varName);
}

-(NSString*)getName {
    NSLog(@"SET NOMBRE %@", name);
    return name;
}

-(void)setPass:(NSString*)varPass {
    pass = varPass;
    NSLog(@"SET PASSWORD %@", varPass);
}

-(NSString*)getPass {
    NSLog(@"SET PASSWORD %@", pass);
    return pass;
}


@end
