//
//  ViewController.h
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 11/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController {
    IBOutlet UITextField *tfUser;
    IBOutlet UITextField *tfPassword;
    IBOutlet UILabel *lblQuestion;
    IBOutlet UIButton *btnLogIn;
    IBOutlet UIButton *btnRegistrate;
    IBOutlet UIImageView *image;

}


@end

