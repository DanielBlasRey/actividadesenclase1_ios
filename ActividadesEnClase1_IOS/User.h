//
//  User.h
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 14/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject {
    NSString *mail;
    NSString *name;
    NSString *pass;
}

-(void)setMail:(NSString*)mail;
-(NSString*)getMail;
-(void)setName:(NSString*)name;
-(NSString*)getName;
-(void)setPass:(NSString*)pass;
-(NSString*)getPass;

@end
