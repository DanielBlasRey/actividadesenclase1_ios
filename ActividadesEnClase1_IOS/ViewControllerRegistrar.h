//
//  ViewControllerRegistrar.h
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 12/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewControllerRegistrar : UIViewController {
    IBOutlet UITextField *tfEmail;
    IBOutlet UITextField *tfUser;
    IBOutlet UITextField *tfPassword;
    IBOutlet UIButton *btnAccept;
    IBOutlet UIButton *btnBack;
    IBOutlet UIImageView *image;

}
@end
